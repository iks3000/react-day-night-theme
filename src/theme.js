const lightTheme = {
    body: '#eeeeee',
    text: '#000000',
}

const darkTheme = {
    body: '#363537',
    text: '#66bb6a',
}

export { lightTheme, darkTheme };