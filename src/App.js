import React, { useState } from 'react';
import { ThemeProvider } from 'styled-components';
import { Slider } from './components/slider/slider';
import { Title } from './components/title/title';
import { Planets } from './components/planets/planets';
import { lightTheme, darkTheme } from './theme';
import { GlobalStyles } from './global';

const dataPlanets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptunes'];


const App = () => {
  const storedDarkMode = localStorage.getItem("THEME_MODE");

  const [theme, setTheme] = useState(storedDarkMode);
  localStorage.setItem("THEME_MODE", theme);

  const toggleTheme = () => theme === 'light' ? setTheme('dark') : setTheme('light');

  const [checked, setChecked] = useState(storedDarkMode === 'light' ? false : true);
  const toggle = (value) => !value;

  return (
    <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
      <GlobalStyles />
      <Slider
        changeTheme={toggleTheme}
        checked={checked}
        onChangeTheme={() => setChecked(toggle)}
        />
      <Title
        textTitle='Solar System Planets:'
      />
      <Planets
        dataList={dataPlanets}
      />
    </ThemeProvider>
  );
}

export default App;
