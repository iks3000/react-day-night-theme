import './slider.css';

const Slider = ({ changeTheme, onChangeTheme, checked}) => (
    <label className="switch" htmlFor="checkbox">
        <input
            onClick={changeTheme}
            onChange={ onChangeTheme }
            checked={checked}
            type="checkbox"
            id="checkbox"
            />
        <div className="slider round"></div>
    </label>
);

export { Slider };