import './planets.css';

const Planets = ({dataList}) => (
    <ul className="planets-list">
        {dataList.map((val, index) =>
            <li key={index}>{val}</li>
        )}
    </ul>
);

export { Planets };