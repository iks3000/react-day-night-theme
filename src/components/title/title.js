const Title = ({ style, textTitle }) => (
    <h1
        style={{ color: "#999", fontSize: "19px" }}>
        {textTitle}
    </h1>
)

export { Title };